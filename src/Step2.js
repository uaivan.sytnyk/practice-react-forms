import React from "react";
import { useNavigate } from 'react-router-dom';
import { useData } from "./DataContext";
import { Navigation } from "./components/Navigation";
import { Header } from "./components/Header";
import "./Step2.css";

export const Step2 = () => {

  const { data, setValues } = useData();

  const options = ["Male", "Female", "I prefer not to say", 'Other'];
  const active =
    (data.gender && options.every((el) => el.toLowerCase() !== data.gender.toLowerCase())) ||
    data.gender === "Other";

  const history = useNavigate();

  const handle = (e) => {
    e.preventDefault();
    history("/");
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    history("/step3");
  }

  return (
    <div className="Step2">

      <Header page={true} />

      <form onSubmit={handleSubmit}>

        <input
          name="firstName"
          type="text"
          placeholder="First Name"
          value={data.firstName}
          onChange={setValues}
          required
        />

        <input
          name="secondName"
          type="text"
          placeholder="Last Name"
          value={data.secondName}
          onChange={setValues}
          required
        />

        <div className="Radio" >

          <p>Gender</p>

          {options.map((item) =>
            <div key={item} className={item === "Other" ? "Other" : ""}>
              <input
                type="radio"
                name="gender"
                id={item}
                value={item}
                checked={item === "Other" ? active : data.gender.toLowerCase() === item.toLowerCase()}
                onChange={setValues}
                required
              />

              <label htmlFor={item}>{item}</label>
              {item === "Other" && <input
                name="gender"
                id="Else"
                type="text"
                required
                disabled={!active}
                value={
                  options.some((el) => el.toLowerCase() === data.gender.toLowerCase())
                    ? ""
                    : data.gender
                }

                onChange={(e) => {

                  if (e.target.value === "") {
                    e.target.value = "Other"
                  }

                  setValues(e);
                }}
              />
              }
            </div>
          )}

        </div>

        <div className="Phone">
          <select name="code" value={data.code} onChange={setValues}>
            <option value='+1'>+1</option>
            <option value='+2'>+2</option>
            <option value='+3'>+3</option>
            <option value='+4'>+4</option>
            <option value='+5'>+5</option>
          </select>
          <input
            type="tel"
            pattern="[0-9]{10}"
            name="phone"
            placeholder="Business phone number"
            value={data.phone}
            onChange={setValues}
            required
          />
        </div>

        <Navigation onReturn={handle} text="Continue" />

      </form>

    </div>
  );
}