import React, { createContext, useState, useContext } from 'react'

const DataContext = createContext()

export const DataProvider = ({ children }) => {

  const initialState = {
    Prof: 'Professional',
    firstName: '',
    secondName: '',
    gender: '',
    phone: '',
    code: '+1',
    email: '',
    password: '',
    categories: [],
  }

  const [data, setData] = useState(initialState);
  const [verify, setVerify] = useState(false);

  const passwordCheck = (e) => {

    const regex = /([A-Z])/;

    if ((e.target.value.length > 8 && regex.test(e.target.value))) {
      setVerify(true);
    } else setVerify(false);

  }

  const setValues = ({ target }) => {
    const { type, name, value } = target;
    let check;

    if (type === 'checkbox') {
      check = data.categories;

      if (!check.includes(value)) {
        check.push(value);
      } else {
        check = check.filter((el) => el !== value);
      }

    } else check = value;

    setData(prevData => ({
      ...prevData,
      [name]: check,
    }))
  }

  const clearState = () => {
    setData(initialState);
  }

  return <DataContext.Provider value={{ data, verify, passwordCheck, setValues, clearState }}>
    {children}
  </DataContext.Provider>
}

export const useData = () => useContext(DataContext);