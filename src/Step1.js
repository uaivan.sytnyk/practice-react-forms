import React from "react";
import { useNavigate } from 'react-router-dom';
import "./Step1.css"
import { useData } from "./DataContext";

export const Step1 = () => {

  const { data, setValues } = useData();

  const history = useNavigate();

  const handleForm = (e) => {
    data.Prof ? history("/step2") : console.log("Choose description");
  }

  const handleClick = (e) => {
    e.target = e.currentTarget;
    setValues(e);
  }

  return (
    <div className="Step">

      <h1>Which describes you best?</h1>

      <form onSubmit={handleForm}>

        <button
          type="button"
          name="Prof"
          value={"Homeowner"}
          onClick={handleClick}
          className={data.Prof === "Homeowner" ? "Active" : "Not"}
        >

          <svg width="22" height="18" viewBox="0 0 22 18" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M16 12H18V14H16V12ZM16 8H18V10H16V8ZM16 4H18V6H16V4ZM12.74 4L14 4.84V4H12.74Z" fill="#757575" />
            <path d="M9 0V1.51L11 2.84V2H20V16H16V18H22V0H9Z" fill="#757575" />
            <path d="M7.17 2.7L14 7.25V18H0V7.48L7.17 2.7ZM9 16H12V8.16L7.17 5.09L2 8.38V16H5V10H9V16Z" fill="#757575" />
          </svg>

          <span>Homeowner</span>
          <p>I am a homeowner or interesed in home design. </p>

        </button>

        <button
          type="button"
          name="Prof"
          value="Professional"
          onClick={handleClick}
          className={data.Prof === "Professional" ? "Active" : "Not"}
        >

          <svg width="20" height="18" viewBox="0 0 20 18" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M18 4H14V2L12 0H8L6 2V4H2C0.9 4 0 4.9 0 6V11C0 11.75 0.4 12.38 1 12.73V16C1 17.11 1.89 18 3 18H17C18.11 18 19 17.11 19 16V12.72C19.59 12.37 20 11.73 20 11V6C20 4.9 19.1 4 18 4ZM8 2H12V4H8V2ZM2 6H18V11H13V8H7V11H2V6ZM11 12H9V10H11V12ZM17 16H3V13H7V14H13V13H17V16Z" fill="#757575" />
          </svg>

          <span>Professional</span>
          <p>I offer home improvement services or sell home products.</p>

        </button>

        <div className="but">

          <button type="submit">

            <span>Next</span>

            <svg width="26" height="25" viewBox="0 0 26 25" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M9.72259 17.0968L14.1604 12.3692L9.44242 7.92108L10.8087 6.46871L16.9891 12.2828L11.175 18.4631L9.72259 17.0968Z" fill="white" />
            </svg>

          </button>
        </div>

      </form>

    </div>
  );
}