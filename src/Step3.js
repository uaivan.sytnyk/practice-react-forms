import React from "react";
import { useNavigate } from 'react-router-dom';
import "./Step3.css"
import { useData } from "./DataContext";
import { Navigation } from "./components/Navigation";
import { Header } from "./components/Header";


export const Step3 = () => {

  const categories = ["Economy", "Business", "Trading", "Communications"];

  const { data, verify, passwordCheck, setValues, clearState } = useData();

  const history = useNavigate();

  const onReturn = (data) => {
    history("/step2");
  }

  const handleSubmit = (e) => {

    e.preventDefault();

    if (verify) {
      console.log(data);
      clearState();
      history("/");
    }
  }

  return (
    <div className="Step3">

      <Header page={false} />

      <p>Categories you work with</p>

      <form onSubmit={handleSubmit}>
        <div className="Checkbox" >
          {categories.map((item) =>
            <div key={item}>
              <input
                type="checkbox"
                name="categories"
                value={item}
                onChange={setValues}
                checked={data.categories.includes(item)}
              />
              <label htmlFor={item}>{item}</label>
            </div>
          )}
        </div>

        <input
          type="email"
          id="email"
          placeholder="Email"
          name="email"
          value={data.email}
          onChange={setValues}
          required
        />

        <div className={verify || !data.password ? "" : "input-false"}>
          <input
            type="password"
            id="password"
            placeholder="Password"
            name="password"
            defaultValue={data.password}
            onChange={(e) => {
              passwordCheck(e);
              setValues(e);
            }}
            //onChange={setValues}
            required
          />
          <p className="Under">The password has to be at least 8 characters long and contain at least one upper case letter.</p>
        </div>

        <Navigation onReturn={onReturn} text="Submit" />

      </form>
    </div>
  );
}